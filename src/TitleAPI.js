import React, { Component } from 'react';

import axios from 'axios';
const APIURL = 'http://192.168.56.1:3001/'

class AllTitles extends Component {
    constructor(props) {
        super(props)
        this.state = {
            requestFailed: false
        }
    }

    getDetail(title){


            axios.get(APIURL+title[1]).then ((response) => {
                this.setState({title: response.data})

            })

    }

    componentDidMount() {

        axios.get(APIURL).then ((response) => {
            this.setState({titles: response.data})

        })

        /*.catch( () =>
        this.setState({
            requestFailed: true
        }))

    fetch(APIURL)

        .then(response => {
            if (!response.ok) {
                throw Error("Network request failed")
            }

            return response
        })
        .then(d => {
            d.json()})
        .then(d => {
            this.setState({
                titles: d
            })
        }, () => {
            this.setState({
                requestFailed: true
            })
        })
*/
    }

    render() {

        if (this.state.requestFailed) return <p>Failed!</p>
        if (!this.state.titles) return <p>Loading...</p>
        return (
            <nav>
            <div>

                    {this.state.titles.map(i => {

                        let boundItemClick = this.getDetail.bind(this, i);
                        return <li key={i} onClick={boundItemClick}>{i[0]}</li>
                            /*
                         return <li key={i}><a href={APIURL + i[1]}>{i[0]}</a></li>
                         */
                    })}


            </div>
            </nav>
        )
    }
}

export default AllTitles;